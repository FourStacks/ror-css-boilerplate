# RoR CSS boilerplate - by FourStacks #

*This basic css structure has been created in order to get css authoring for RoR projects up and running quickly
and to improve consistency between projects.  Each file is commented and whilst much of the existing CSS contained
within each file is optional, much of it is generic and could be used across most responsive web applications.  However,
there is no need to use everything included in this boilerplate and if certain tools such as Compass are not required then
you are encourage to fork this boilerplate and adapt it to suit your needs and workflow*


## Prerequisites ##

These steps will only need to be carried out once at the start of the project.  Many of these steps have been taken 
from the article on [55minutes](http://blog.55minutes.com/2013/01/lightning-fast-sass-reloading-in-rails-32/) .  All credit due to
the author Matt Brictson (@mattbrictson).  Your first two prerequisites are:

1) The livereload browser extension (google this to get the extension for you browser of choice)
2) Familiarity (or a lack of fear of!) the command line



###Step 1 - Add required gems###

In the project gemfile add the following gems into the development block (if they are not already there. 
These four gems are require in order to enable livereloads:

```ruby
group :development do
  	gem 'guard', '>= 2.2.2',       :require => false
  	gem 'guard-livereload',        :require => false
  	gem 'rack-livereload'
  	gem 'rb-fsevent',              :require => false
end
```

In the assets block add the following gems.  At time of writing the following versions work well together.


```ruby
group :assets do
  gem 'coffee-rails', '~> 3.2.1'
  gem 'sass-rails'
  gem 'compass-rails', '~> 2.0.0'
  gem 'compass', '~> 1.0.0.alpha.21'
  gem 'breakpoint'
  gem 'susy'
  gem 'uglifier', '>= 1.0.3'
end
```


The susy and breakpoint gems are optional though if the project requires a grid system or will be responsive 
these two gems are highly encouraged.  Please see the relevant documentation for each:

- [http://susydocs.oddbird.net/]
- [http://breakpoint-sass.com/]



###Step 2 - configure app for livereloads###

Add the following line to config/environments/development.rb

```ruby
YourAppName::Application.configure do
  # Automatically inject JavaScript needed for LiveReload
  config.middleware.insert_after(ActionDispatch::Static, Rack::LiveReload)
end
```

(note that it’s only the line underneath the comment that is required the do/end lines should already be present)



###Step 3 - Add Guardfile configuration###

Within your apps Guardfile add the following code block (if your app does not have a guardfile, just create a blank file in your project root and call it Guardfile with a capital G and no extension)

```ruby
guard 'livereload' do
  watch(%r{app/views/.+\.(erb|haml|slim)$})
  watch(%r{app/helpers/.+\.rb})
  watch(%r{public/.+\.(css|js|html)})
  watch(%r{config/locales/.+\.yml})
  # Rails Assets Pipeline
  watch(%r{(app|vendor)(/assets/\w+/(.+\.(css|js|html))).*}) { |m| "/assets/#{m[3]}" }
end
```


###Step 4 - Add font-url helper###

By default compass rails will include helpers such as the image-url helper meaning that you can add declarations such 
as background: image-url(‘my-image.png’) - and not have to worry about the exact path of the image as long as it 
in app/assets/images

If you wish to use fonts in the project via @font-face however you will need to add support for the font-url helper 
by adding the following line to app/config/application.rb

```ruby
module Biodiversity
  class Application < Rails::Application

    config.assets.paths << Rails.root.join("app", "assets", "fonts")

  end
end
```

(Note the first two lines and last two lines should already be present and are only included for context - 
simply copy/paste the middle line into the correct part of application.rb

Now, if you create a folder called fonts at app/assets and use the font-url helper the app should pick up the font files.



## During development ##

###Running Guard###

If you have configured the app to use livereloads, when you are ready to start developing open a new terminal window alongside one running the rails server and when in the project directory run:

`guard -P livereload

This will prompt you to connect a browser so simply click the livereload browser extension button in your browser of choice and guard should report that a connected browser has been found.  Guard will now watch your css, js and html based files for changes and whena change has been detected your browser window will be reloaded

###Creating new CSS partials###

In order for Guard to pick up on chnages being made within your CSS on save, you must ensure that all CSS files are saved with the suffix css.scss  (e.g. _my-file.css.scss).  Similarly any new markup partials you create should be saved with the suffix .html.erb so that changes made to view markup will also be live reloaded